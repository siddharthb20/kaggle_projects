# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 10:25:39 2021

@author: Siddharth
"""

from sklearn.ensemble import RandomForestClassifier
from titanic_loading import data_pre_process
from sklearn.model_selection import GridSearchCV, cross_val_score

X_train, X_test, y_train, y_test = data_pre_process()

#random_forest = RandomForestClassifier(n_estimators = 100)
#random_forest.fit(X_train, y_train)

#y_pred = random_forest.predict(X_test)

#param_grid = { "criterion" : ["gini", "entropy"], "min_samples_leaf" : [1, 5, 10, 25, 50, 70], "min_samples_split" : [2, 4, 10, 12, 16, 18, 25, 35], "n_estimators": [100, 400, 700, 1000, 1500]}

#Grid search to find the optimum hyperparameters
#rf = RandomForestClassifier(n_estimators=100, max_features='auto', oob_score=True, random_state=1, n_jobs=-1)
#clf = GridSearchCV(estimator=rf, param_grid=param_grid, n_jobs=-1)
#clf.fit(X_train, y_train)
#print(clf.best_params_) 
#print(X_train)

random_forest = RandomForestClassifier(criterion = "gini", 
                                       min_samples_leaf = 1, 
                                       min_samples_split = 10,   
                                       n_estimators=100, 
                                       max_features='auto', 
                                       oob_score=True, 
                                       random_state=1, 
                                       n_jobs=-1)

random_forest.fit(X_train, y_train)

y_pred = random_forest.predict(X_test)

print(random_forest.score(X_test, y_test))