# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 18:43:07 2021

@author: Siddharth
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 22:35:31 2021

@author: Siddharth
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
from sklearn.metrics import accuracy_score, mean_absolute_error
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
import kerastuner as kt
import tensorflow as tf
from titanic_loading import data_pre_process


X_train, X_test, y_train, y_test = data_pre_process()
#print(list(map(tuple, np.where(np.isnan(X_train)))))


BATCH_SIZE = 16
EPOCHS = 200

def model():
    
    model = Sequential([
        Dense(32, input_dim=10, activation='relu'),
        Dense(64, activation='relu'),
        Dense(64, activation='relu'),
        Dense(2, activation='linear')])
    
    model.summary()
    
    model.compile(loss='mean_absolute_error', optimizer='adam', metrics=['accuracy'])
    
    return model

clf = KerasClassifier(build_fn = model, batch_size = BATCH_SIZE, epochs = EPOCHS)

results = clf.fit(X_train, y_train)

print(clf.score(X_test, y_test))