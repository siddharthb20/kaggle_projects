# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 22:35:31 2021

@author: Siddharth
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.decomposition import PCA
import re


def data_pre_process():
    df = pd.read_csv('train.csv')
    
    #df.fillna(0, inplace=True)
    
    #Sex
    df.loc[df['Sex'] == 'male', 'Sex'] = 0
    df.loc[df['Sex'] == 'female', 'Sex'] = 1
    
    #Embarked
    df.loc[df['Embarked'] == 'S', 'Embarked'] = 0
    df.loc[df['Embarked'] == 'C', 'Embarked'] = 1
    df.loc[df['Embarked'] == 'Q', 'Embarked'] = 2
    most_common_origin = df['Embarked'].value_counts().argmax()
    df['Embarked'] = df['Embarked'].fillna(most_common_origin)
    
    #Age
    mean_age = df['Age'].mean()
    std_dev_age = df['Age'].std()
    null_values_age = df['Age'].isna().sum()
    rand_age = np.random.randint(mean_age - std_dev_age, mean_age + std_dev_age, size = null_values_age)
    age_slice = df["Age"].copy()
    age_slice[np.isnan(age_slice)] = rand_age
    df["Age"] = age_slice.astype(int)
    
    print(df['Fare'].max())
    
    #Cabin
    deck = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "U": 8}
    df['Cabin'] = df['Cabin'].fillna("U0")
    #print(df['Cabin'])
    df['Deck'] = df['Cabin'].map(lambda x: re.compile("([a-zA-Z]+)").search(x).group())
    df['Deck'] = df['Deck'].map(deck)
    df['Deck'] = df['Deck'].fillna(0)
    df['Deck'] = df['Deck'].astype(int)

    #Fare
    #df['Fare'] = df['Fare'].fillna(0)
    #df['Fare'] = df['Fare'].astype(int)
    
    #Extracting survived column as labels
    y_labels = df[['Survived']].values
    
    #Age_class
    df['Age_Class']= df['Age'] * df['Pclass']
    
    #Names
    #print(df['Name'])
    df['Title'] = df.Name.str.extract(' ([A-Za-z]+)\.', expand=False)    
    df['Title'] = df['Title'].replace(['Lady', 'Countess','Capt', 'Col','Don', 'Dr', 'Major', 'Rev', 'Jonkheer', 'Dona', 'Sir'], 'Misc')
    df['Title'] = df['Title'].replace(['Mlle', 'Ms'], 'Miss')
    df['Title'] = df['Title'].replace('Mme', 'Mrs')
    titles = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Misc": 5}
    df['Title'] = df['Title'].map(titles)
    #print(df['Title'])
    
    #print(df.columns)
    
    #Dropping columns not needed
    drop_cols = df.drop(['PassengerId', 'Survived', 'Name', 'Ticket', 'Cabin'], axis=1).values
    data = drop_cols
    
    pca = PCA(n_components=7)
    #data = pca.fit_transform(drop_cols, y_labels)
    
    X_train, X_test, y_train, y_test = train_test_split(data, y_labels, test_size=0.33, random_state=24)
    y_train = y_train.ravel()
    y_test = y_test.ravel()
    
    return X_train, X_test, y_train, y_test

X_train, X_test, y_train, y_test = data_pre_process()

#clf = make_pipeline(StandardScaler(), SVC(C=0.9, gamma='auto'))

#clf.fit(X_train, y_train)

#y_pred = clf.predict(X_test)

#print(clf.score(X_test, y_test)*100)
